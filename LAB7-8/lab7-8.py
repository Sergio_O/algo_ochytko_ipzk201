print('Лабараторна робота 7-8')
isEnable = [bool] * 19  # перевірка чи були ми там
lenght = [[0] * 19 for i in range(19)]  # масив
lenght[0][1] = 135
lenght[0][10] = 78
lenght[0][16] = 128
lenght[1][2] = 80
lenght[1][5] = 38
lenght[1][9] = 115
lenght[2][3] = 100
lenght[3][4] = 68
lenght[5][6] = 73
lenght[6][7] = 110
lenght[7][8] = 104
lenght[10][11] = 146
lenght[10][13] = 115
lenght[10][14] = 181
lenght[11][12] = 105
lenght[14][15] = 130
lenght[16][17] = 175
lenght[16][18] = 109
count = 0
city = [['Київ'], ['Житомир'], ['Новоград-Волинський'], ['Рівне'], ['Луцьк'], ['Бердичів'], ['Вінниця'],
        ['Хмельницький'], ['Тернопіль'], ['Шепетівка'], ['Біла церква'], ['Черкаси'], ['Кременчуг'], ['Умань'],
        ['Полтава'], ['Харків'], ['Прилуки'], ['Суми'], ['Миргород']]


def DFS(count, string, res):
    if count == 0:  # перший перехід
        ways = string + str(city[count])
    if count != 0:  # наступні переходи
        ways = string + ' > ' + str(city[count])
    if res != 0:  # Якщо в першому перхід додаємо до тексту відстань
        ways += ' ' + str(res) + ' км '
    print(ways)

    for i in range(19):  # цикл для рекурсії
        if isEnable[i] != False and lenght[count][i] != 0:
            DFS(i, ways, res=lenght[count][i])


visited = []
queue = []


def BFS(visited, lenght, city, time, queue, string):
    visited.append(city[time])  # додаємо в список пройдений
    queue.append(city[time])  # додаємо в чергу
    time += 1

    while queue:  # Якщо черга не пуста
        if time > 0:
            string += " > " + str(queue.pop(0))
        else:
            string += str(queue.pop(0))
    print(string)

    for neighbour in range(len(lenght)):
        if city[neighbour] not in visited:
            visited.append(city[neighbour])
            queue.append(city[neighbour])
            BFS(visited, lenght, city, time + 1, queue, string)


bool1 = True
while bool1:
    try:
        x = int(input('Введіть якою функцією вивести: 1. DFS, 2.BFS: '))
        bool1 = False
    except:
        print("Невірне значення!")
        bool1 = True
if x == 1:
    DFS(count, "", 0)  # запуск обхода в глубину
elif x == 2:
    BFS(visited, lenght, city, 0, queue, '')  # запуск обхода в ширину
else:
    exit(0)  # вихід