print('Додаткова робота')
#Імпортую socket модуль
import socket

def xor(a, b):
    # ініціалізувати cписок результат
    result = []

    # Обхід усіх бітів, якщо біти є
    # те саме, тоді XOR дорівнює 0, інакше 1
    for i in range(1, len(b)):
        if a[i] == b[i]:
            result.append('0')
        else:
            result.append('1')

    return ''.join(result)


# Виконує поділ за модулем-2
def mod2div(divident, divisor):
    # Кількість бітів
    pick = len(divisor)

    # Нарізання дільника відповідно
    # до довжини для певного кроку
    tmp = divident[0: pick]

    while pick < len(divident):

        if tmp[0] == '1':

            # замінити дільник на результат
            # XOR і потягніть на 1 біт вниз
            tmp = xor(divisor, tmp) + divident[pick]

        else:  # Якщо крайній лівий біт - '0'

            # Якщо крайній лівий біт дивіденду (або
            # частина, що використовується на кожному кроці) дорівнює 0, крок не може
            # використовувати регулярний дільник; нам потрібно використовувати
            # дільник усіх 0.
            tmp = xor('0' * pick, tmp) + divident[pick]

        # інкремент, щоб рухатися далі
        pick += 1

    # Для останніх n бітів ми повинні це виконати
    # зазвичай, оскільки це спричиняє збільшення значення вибору
    # Індекс поза межами.
    if tmp[0] == '1':
        tmp = xor(divisor, tmp)
    else:
        tmp = xor('0' * pick, tmp)

    checkword = tmp
    return checkword


# Функція, яка використовується на стороні відправника для кодування
# даних шляхом додавання залишку модульного поділу
# в кінці даних.
def encodeData(data, key):
    l_key = len(key)

    # Додає n-1 нулі в кінці даних
    appended_data = data + '0' * (l_key - 1)
    remainder = mod2div(appended_data, key)

    # Додайте залишок до вихідних даних
    codeword = data + remainder
    return codeword


# Створення об'єкта сокета
s = socket.socket()
print ("Socket успішно створено!")
# Визначте порт, через який ви хочете підключитися
port = 1025
# підключитися до сервера на локальному комп’ютері
s.connect(('127.0.0.1', port))
input_string = input("Введіть дані, які потрібно надіслати->")
s.sendall(input_string)
data = (''.join(format(ord(x), 'b') for x in input_string))
print(data)
key = "1001"
ans = encodeData(data, key)
print(ans)
if len(ans) == len(data) + 3:
    s.sendall("Дякую! Ваша дані: " + data + " Отримано Не знайдено помилки.")
# закрити з'єднання
s.close()
